<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Buat Account Baru</h2>
    <h3>Sign Up Form</h3>

    <form action="/register" method="post">
    @csrf
        <label for="">First Name:</label><br>
        <input type="text" name="first_name"><br><br>
        <label for="">Last Name:</label><br>
        <input type="text" name="last_name"><br><br>
        
        <label for="">Gender:</label><br><br>
        <input type="radio" name="gender" value="Man">Man<br>
        <input type="radio" name="gender" value="Women">Women<br>
        <input type="radio" name="gender" value="Other">Other<br><br>

        <label for="">Nationality:</label><br><br>
        <select name="nationality">
            <option value="Indonesia">Indonesian</option>
            <option value="English">Malaysia</option>
            <option value="Singapore">Singapore</option>
        </select><br><br>

        <label for="">Language Spoken:</label><br><br>
        <input type="checkbox" name="language_spoken" value="Bahasa Indonesia">Bahasa Indonesia<br>
        <input type="checkbox" name="language_spoken" value="English">English<br>
        <input type="checkbox" name="language_spoken" value="Other">Other<br><br>

        <label for="">Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>

    <!-- Tambahkan isian lainnya sesuai kebutuhan -->
        <input type="submit" value="Sign Up">
    </form>
    

</body>
</html>
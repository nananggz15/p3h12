<h1>Welcome</h1>
<p>First Name: {{ $first_name }}</p>
<p>Last Name: {{ $last_name }}</p>
<p>Gender: {{ $gender }}</p>
<p>Nationality: {{ $nationality }}</p>
<p>Language Spoken: {{ $language_spoken }}</p>
<p>Bio: {{ $bio }}</p>
<!-- Tampilkan data lainnya jika ada -->

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function show()
    {
        return view('register');
    }

    public function register(Request $request)
    {
        $first_name = $request['first_name'];
        $last_name = $request['last_name'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $language_spoken = $request['language_spoken'];
        $bio = $request['bio'];
        // return view('welcome', compact('first_name', 'last_name', 'gender', 'cars', 'skill', 'message'));

        return view('welcome',['first_name'=>$first_name, 'last_name'=>$last_name, 'gender'=>$gender, 'nationality'=>$nationality, 'language_spoken'=>$language_spoken, 'bio'=>$bio]);
    }

    // public function welcome(Request $request)
    // {
    //     $first_name = $request->input('first_name');
    //     $last_name = $request->input('last_name');

    //     return view('welcome', compact('first_name', 'last_name'));
    // }
}
